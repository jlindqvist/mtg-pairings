(defproject mtg-pairings "0.2.0"
  :license {:name "MIT License"
            :url "http://www.opensource.org/licenses/mit-license.php"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [clojess "0.3"]
                 [seesaw "1.4.4"]
                 [watchtower "0.1.1"]
                 [http-kit "2.1.16"]
                 [org.clojure/tools.reader "0.8.3"]
                 [cheshire "5.3.1"]
                 [clj-time "0.6.0"]]
  :profiles {:uberjar {:main mtg-pairings.core
                       :aot [mtg-pairings.core]}})
